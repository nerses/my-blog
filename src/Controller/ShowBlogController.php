<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShowBlogController extends AbstractController {

    /**
     * @Route("/all", name="show_all")
     */
    public function index(PostRepository $repo) { 

        $blogs = $repo->findAll();
        
        return $this->render('show-blog.html.twig', [
            'blogs' => $blogs
        ]);
    }



    /**
     * @Route("/person/{id}", name="one_person")
     */
    public function onePerson(int $id) { 
        $repo = new PostRepository();
        $person = $repo->findById($id);
      
        return $this->render('one-blog.html.twig', [
            'blog' => $person
        ]);
    }





}