<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="add_blog")
     */
    public function addText(Request $request)
    {   
        $emptytext = null;  
        
        $title = $request->get("title");
        $autor = $request->get("autor");
        $content = $request->get("content"); 
        $data = $request->get("data");       
       
        if ($title && $autor && $content && $data) {
        
            $data =  DateTime::createFromFormat('d/m/y', $data);    
            $emptytext = new Post($title, $autor, $data, $content);
            $repo = new PostRepository(); 
            $repo->add($emptytext);
        }

        return $this->render("index.html.twig", [
            'blog' => $emptytext
        ]);

    }


    














}
