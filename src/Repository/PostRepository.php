<?php

namespace App\Repository;

use App\Entity\Post;


class PostRepository
{
    private $pdo;

    public function __construct() {
 
        $this->pdo = new \PDO(
            'mysql:host=localhost;dbname=bfmeBlog',
            'simplon',
            '1234'
        );
    }

    /**
     * @return Post[]
     */
    public function findAll(): array
    {
       
        $query = $this->pdo->prepare('SELECT * FROM post');
       
        $query->execute();

        $results = $query->fetchAll();
        $list = [];
       
        foreach ($results as $line) {

            $post = $this->sqlToPost($line);
            $list[] = $post;
        }
    
        return $list;
    }

    public function add(Post $post): void {

        $query = $this->pdo->prepare('INSERT INTO post (title, author, content) VALUES (:title, :author, :content)');

        // $query->bindValue('id', $post->getId(), \PDO::PARAM_INT);
        $query->bindValue('title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue('author', $post->getAuthor(), \PDO::PARAM_STR);
        // $query->bindValue('postDate', $post->getPostDate()->format('Y-m-d H:i:s'));
        $query->bindValue('content', $post->getContent(), \PDO::PARAM_STR);
       
        $query->execute();

        $post->setId(intval($this->pdo->lastInsertId()));
    }

    /**
     * Méthode permettant de récupérer une personne spécifique en utilisant
     * son id. Si la personne n'existe pas, on renvoie null
     */
    public function findById(int $id): ?Post {
        //On fait la requête SELECT mais avec un WHERE pour l'id cette fois
        $query = $this->pdo->prepare('SELECT * FROM post WHERE id=:idPlaceholder');
        //On assigne au placeholder la valeur contenu dans l'argument
        //id de la méthode
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        //On exécute la requête
        $query->execute();
        //On récupère le premier résultat de la requête
        $line = $query->fetch();
        //Si ce résultat existe bien
        if($line) {
            //Alors on renvoie l'instance de Person correspondante
            return $this->sqlToPost($line);
            //return new Person($line['name'], $line['personality'], $line['age'], $line['id']);
        }
        //Sinon on renvoie null pour indiquer qu'aucune personne ne
        //correspondait à l'id fourni
        return null;

    }

    private function sqlToPost(array $line):Post {
        return new Post($line['title'], $line['author'], new \DateTime($line['postDate']), $line['content'], $line['id']);
    }
}
