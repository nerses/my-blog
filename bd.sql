-- On supprime la base de données si elle existe
DROP DATABASE IF EXISTS bfmeBlog;
-- On crée la base de données introduction
CREATE DATABASE bfmeBlog;
-- On sélectionne la base de données introduction
USE bfmeBlog;

-- On crée une table Post
CREATE TABLE post( 
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    title VARCHAR(64),
    author VARCHAR(68) ,
    content VARCHAR(128),
    postDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP

);
INSERT INTO post (title, author, content) VALUES ('pas content', 'flo','bonjour à tous je test des trucs');
INSERT INTO post (title, author, content) VALUES ('coucou', 'fabien','bonjour je suis fabien et je suis heureux');
INSERT INTO post (title, author, content) VALUES ('Paula', 'gill', 'dernier test pour gill post');

SELECT * FROM post;